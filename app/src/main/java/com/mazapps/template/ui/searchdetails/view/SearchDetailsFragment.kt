package com.mazapps.template.ui.searchdetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.mazapps.template.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_search_details.*

@AndroidEntryPoint
class SearchDetailsFragment : Fragment() {

    private val args: SearchDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_search_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        title.text = args.searchData.title
        wordCount.text = args.searchData.wordcount.toString()
        pageId.text = args.searchData.pageid.toString()
        size.text = args.searchData.size.toString()
    }
}