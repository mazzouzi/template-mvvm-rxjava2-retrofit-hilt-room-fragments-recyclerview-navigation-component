package com.mazapps.template.ui.searchinfo.viewmodel

import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.data.CallStateEnum.*
import com.mazapps.template.data.DataManager
import com.mazapps.template.data.model.Search
import com.mazapps.template.data.model.Wikipedia
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Response

/**
 * @author morad.azzouzi on 11/11/2020.
 */
class SearchInfoViewModel(
    private val disposables: CompositeDisposable,
    private val dataManager: DataManager
) {

    val state: BehaviorSubject<CallStateEnum> = BehaviorSubject.createDefault(IDLE)
    val offlineState: BehaviorSubject<CallStateEnum> = BehaviorSubject.createDefault(IDLE)

    var items: MutableList<Pair<SearchInfoEnum, Any>> = mutableListOf()
    val itemCount: Int
        get() = items.size
    fun getItemViewTypeAt(position: Int): Int = items[position].first.ordinal
    fun getItemDataAt(position: Int): Any = items[position].second

    fun fetchSearchInfo(): Disposable {
        state.onNext(IN_PROGRESS)

        return dataManager
            .fetchSearchInfo("query", "json", "search", KEYWORD)
            .subscribe(
                { handleFetchSearchInfoResponse(it) },
                { handleFetchSearchInfoError() }
            )
    }

    private fun handleFetchSearchInfoResponse(response: Response<Wikipedia>) {
        val body = response.body()
        if (response.isSuccessful && body != null) {
            handleFetchSearchInfoSuccess(body)
        } else {
            handleFetchSearchInfoError()
        }
    }

    private fun handleFetchSearchInfoSuccess(body: Wikipedia) {
        body.query.search.also {
            createRelatedSearchData(it)
            storeRelatedSearchInDatabase(it)
        }

        state.onNext(SUCCESS)
        state.onNext(IDLE)
    }

    private fun createRelatedSearchData(search: List<Search>) {
        search
            .mapTo(items) { Pair(SearchInfoEnum.SEARCH, it) }
            .add(0, Pair(SearchInfoEnum.HEADER, KEYWORD))
    }

    private fun storeRelatedSearchInDatabase(search: List<Search>) {
        disposables.add(insertRelatedSearch(search))
    }

    private fun insertRelatedSearch(search: List<Search>): Disposable =
        Observable
            .fromIterable(search)
            .flatMap { dataManager.insert(it).toObservable() }
            .subscribeOn(Schedulers.io())
            .subscribe({}, {})

    private fun handleFetchSearchInfoError() {
        state.onNext(ERROR)
        state.onNext(IDLE)
    }

    fun fetchSearchInfoFromDatabase(): Disposable {
        offlineState.onNext(IN_PROGRESS)

        return dataManager
            .fetchRelatedSearch()
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    if (it.isNotEmpty()) {
                        createRelatedSearchData(it)
                        offlineState.onNext(SUCCESS)
                        offlineState.onNext(IDLE)
                    } else {
                        offlineState.onNext(ERROR)
                        offlineState.onNext(IDLE)
                    }
                },
                {
                    offlineState.onNext(ERROR)
                    offlineState.onNext(IDLE)
                }
            )
    }

    companion object {
        const val KEYWORD = "sasuke"
    }
}