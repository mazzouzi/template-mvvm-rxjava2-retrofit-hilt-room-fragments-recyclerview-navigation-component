package com.mazapps.template.ui.searchinfo.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.data.model.Search
import com.mazapps.template.helper.RxBusHelper
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_search_info.*

/**
 * @author morad.azzouzi on 22/11/2020.
 */
class SearchInfoHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    init {
        itemView.setOnClickListener {
            RxBusHelper.setEvent(SearchInfoClickEvent(adapterPosition))
        }
    }

    fun bind(search: Search) {
        title.text = search.title
    }
}