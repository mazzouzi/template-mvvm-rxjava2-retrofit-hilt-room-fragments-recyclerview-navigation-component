package com.mazapps.template.ui.searchinfo.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.ViewFlipper
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.mazapps.template.R
import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.data.model.Search
import com.mazapps.template.helper.RxBusHelper
import com.mazapps.template.ui.interfaces.LoadingFlipperInterface
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_search_info.*
import javax.inject.Inject

@AndroidEntryPoint
class SearchInfoFragment : Fragment(), LoadingFlipperInterface {

    @Inject lateinit var viewModel: SearchInfoViewModel
    @Inject lateinit var disposables: CompositeDisposable
    @Inject lateinit var adapter: SearchInfoAdapter
    @Inject lateinit var itemDecoration: DividerItemDecoration

    override val viewFlipper: ViewFlipper
        get() = searchViewFlipper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_search_info, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerEvent()
        observeStates()
        initView()
    }

    private fun observerEvent() {
        disposables.add(
            RxBusHelper
                .events
                .subscribe {
                    when (it) {
                        is SearchInfoClickEvent -> onSearchInfoItemClick(it)
                    }
                }
        )
    }

    private fun onSearchInfoItemClick(event: SearchInfoClickEvent) {
        val itemData = viewModel.getItemDataAt(event.position) as Search
        val action = SearchInfoFragmentDirections.actionSearchInfoToSearchDetails(itemData)
        findNavController().navigate(action)
    }

    private fun observeStates() {
        disposables.add(
            viewModel
                .state
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        CallStateEnum.SUCCESS -> onFetchSearchInfoSuccess()
                        CallStateEnum.ERROR -> onFetchSearchInfoError()
                        else -> {
                            // do nothing
                        }
                    }
                }
        )

        disposables.add(
            viewModel
                .offlineState
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        CallStateEnum.SUCCESS -> onFetchSearchInfoSuccess()
                        CallStateEnum.ERROR -> onFetchRelatedSearchFromDatabaseError()
                        else -> {
                            // do nothing
                        }
                    }
                }
        )
    }

    private fun onFetchSearchInfoSuccess() {
        showContent()
        adapter.notifyDataSetChanged()
    }

    private fun onFetchSearchInfoError() {
        disposables.add(viewModel.fetchSearchInfoFromDatabase())
    }

    private fun onFetchRelatedSearchFromDatabaseError() {
        Toast.makeText(context, getString(R.string.error_occured), Toast.LENGTH_SHORT).show()
        showNoResultView()
        adapter.notifyDataSetChanged()
    }

    private fun initView() {
        recyclerView.setHasFixedSize(true)
        recyclerView.addItemDecoration(itemDecoration)
        recyclerView.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        if (viewModel.items.isEmpty()) {
            disposables.add(viewModel.fetchSearchInfo())
        }  else {
            showContent()
            adapter.notifyDataSetChanged()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.clear()
    }
}