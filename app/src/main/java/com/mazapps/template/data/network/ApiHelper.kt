package com.mazapps.template.data.network

import com.mazapps.template.data.model.Wikipedia
import io.reactivex.Single
import retrofit2.Response

/**
 * @author morad.azzouzi on 01/12/2019.
 */
interface ApiHelper {

    fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Single<Response<Wikipedia>>
}