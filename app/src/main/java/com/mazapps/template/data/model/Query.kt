package com.mazapps.template.data.model

data class Query(
    val search: List<Search>,
    val searchinfo: Searchinfo
)